<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313092223 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'advertisement-background.overview', 'hash' => '59980b43d8e610e8048c392a6cf73f6b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'BG reklamy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.advertisement-background.title', 'hash' => '7e14f50fa9453635192ac9de717a651d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'BG reklamy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.advertisement-background.description', 'hash' => '7ae6ee46a85ad50fcd5d1bde1ecfa591', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat reklamy na pozadí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'advertisement-background.overview.title', 'hash' => '526c1cd739e6907dd1c3e579cbb9d1d0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Reklamy na pozadí|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement-background.overview.action.new', 'hash' => '60ffd4c95b6771cb0a2a0ac10e178b4b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit reklamu na pozadí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement-background.overview.name', 'hash' => '3b8b84d8c2e1d5d5ba80ee2e948ad04a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement-background.overview.website', 'hash' => '35af46e2dc10c225c4f3f77491098fb6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkazy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement-background.overview.is-active', 'hash' => '841aca18e9ac6ca8dba26c879f266c80', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'advertisement-background.edit.title', 'hash' => '8ea1b41ae06d59874e3f57ed9c04b495', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení reklamy na pozadí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.name', 'hash' => '2db477af1d5ed8e1cf2be9c41360ff0d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.name.req', 'hash' => 'a9bc5792f72cb14b30fd094d28bd07ac', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.website', 'hash' => '7fa56809598502a484966181b59db8e4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hlavní odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.website.description', 'hash' => '8e42ae6e52ae86c5b4d5010512f62f99', 'module' => 'admin', 'language_id' => 1, 'singular' => 'použije se místo prázdných odkazů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.website-left', 'hash' => 'fae5f27332cf5908cf2c553d1aec9405', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Levý odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.website-right', 'hash' => '6f553f12bf7c8491cb6524c214fca399', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pravý odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.image-preview', 'hash' => '6540e332f8923db11d248a987263c852', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled reklamy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.image-preview.rule-image', 'hash' => 'c19d5925b1b90aef3165d2b8d7a33952', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.is-active', 'hash' => 'e1c150d534bfc1fc64665e20b7202fcc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.send', 'hash' => 'ae893378bdd602aefd785cec82f18478', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.send-back', 'hash' => '4c042ccb7cb8e757b894ec1c84929f11', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.back', 'hash' => '8ee4166f4fdf8b5e4fcae46809e16269', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'advertisement-background.edit.title - %s', 'hash' => '386f3800758503f6ab688b833011376e', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace reklamy na pozadí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.flash.success.create', 'hash' => '3096cd862c216e0f67697ea5a7b68e5a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Reklama na pozadí byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement-background.edit.flash.success.update', 'hash' => 'e684cd150953421eb4b6f769713d5b2d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Reklama na pozadí byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement-background.overview.action.edit', 'hash' => 'a32a4a069af29567810af37f36a51e95', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
