<?php

declare(strict_types=1);

namespace Skadmin\AdvertisementBackground;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'advertisement-background';
    public const DIR_IMAGE = 'background-adv'; // blockery blokovali "advertisement-background"

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-ad']),
            'items'   => ['overview'],
        ]);
    }
}
