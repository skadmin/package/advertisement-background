<?php

declare(strict_types=1);

namespace Skadmin\AdvertisementBackground\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\AdvertisementBackground\BaseControl;
use Skadmin\AdvertisementBackground\Doctrine\Advertisement\AdvertisementBackground;
use Skadmin\AdvertisementBackground\Doctrine\Advertisement\AdvertisementBackgroundFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory                 $webLoader;
    private AdvertisementBackgroundFacade $facade;
    private AdvertisementBackground       $advertisementBackground;
    private ImageStorage                  $imageStorage;

    public function __construct(?int $id, AdvertisementBackgroundFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->advertisementBackground = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->advertisementBackground->isLoaded()) {
            return new SimpleTranslation('advertisement-background.edit.title - %s', $this->advertisementBackground->getName());
        }

        return 'advertisement-background.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->advertisementBackground->isLoaded()) {
            if ($identifier !== null && $this->advertisementBackground->getImagePreview() !== null) {
                $this->imageStorage->delete($this->advertisementBackground->getImagePreview());
            }

            $advertisement = $this->facade->update(
                $this->advertisementBackground->getId(),
                $values->name,
                $values->is_active,
                $values->website,
                $values->website_left,
                $values->website_right,
                $identifier
            );
            $this->onFlashmessage('form.advertisement-background.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $advertisement = $this->facade->create(
                $values->name,
                $values->is_active,
                $values->website,
                $values->website_left,
                $values->website_right,
                $identifier
            );
            $this->onFlashmessage('form.advertisement-background.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $advertisement->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->advertisementBackground = $this->advertisementBackground;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.advertisement-background.edit.name')
            ->setRequired('form.advertisement-background.edit.name.req');
        $form->addText('website', 'form.advertisement-background.edit.website');
        $form->addText('website_left', 'form.advertisement-background.edit.website-left');
        $form->addText('website_right', 'form.advertisement-background.edit.website-right');
        $form->addCheckbox('is_active', 'form.advertisement-background.edit.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('image_preview', 'form.advertisement-background.edit.image-preview');

        // BUTTON
        $form->addSubmit('send', 'form.advertisement-background.edit.send');
        $form->addSubmit('send_back', 'form.advertisement-background.edit.send-back');
        $form->addSubmit('back', 'form.advertisement-background.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->advertisementBackground->isLoaded()) {
            return [];
        }

        return [
            'name'          => $this->advertisementBackground->getName(),
            'is_active'     => $this->advertisementBackground->isActive(),
            'website'       => $this->advertisementBackground->getWebsite(),
            'website_left'  => $this->advertisementBackground->getWebsiteLeft(true),
            'website_right' => $this->advertisementBackground->getWebsiteRight(true),
        ];
    }
}
