<?php

declare(strict_types=1);

namespace Skadmin\AdvertisementBackground\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
