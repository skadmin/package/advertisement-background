<?php

declare(strict_types=1);

namespace Skadmin\AdvertisementBackground\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\AdvertisementBackground\BaseControl;
use Skadmin\AdvertisementBackground\Doctrine\Advertisement\AdvertisementBackground;
use Skadmin\AdvertisementBackground\Doctrine\Advertisement\AdvertisementBackgroundFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

use function sprintf;

class Overview extends GridControl
{
    use APackageControl;

    private AdvertisementBackgroundFacade $facade;

    public function __construct(AdvertisementBackgroundFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'advertisement-background.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator): string {
            return $translator->translate($text);
        });

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.advertisement-background.overview.name')
            ->setRenderer(function (AdvertisementBackground $advertisementBackground): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $advertisementBackground->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($advertisementBackground->getName());

                return $name;
            });
        $grid->addColumnText('website', 'grid.advertisement-background.overview.website')
            ->setTemplateEscaping(false)
            ->setRenderer(static function (AdvertisementBackground $advertisementBackground): ?Html {
                if ($advertisementBackground->getWebsite() === '' && $advertisementBackground->getWebsiteLeft() === '' && $advertisementBackground->getWebsiteRight() === '') {
                    return null;
                }

                $dataWebsite = [
                    'head'  => $advertisementBackground->getWebsite(),
                    'left'  => $advertisementBackground->getWebsiteLeft(true),
                    'right' => $advertisementBackground->getWebsiteRight(true),
                ];

                $icon = Html::el('small', ['class' => 'fas fa-external-link-alt mx-1']);

                $websites = new Html();
                foreach ($dataWebsite as $type => $website) {
                    if ($website === '') {
                        continue;
                    }

                    $href = Html::el('a', [
                        'href'   => $website,
                        'target' => '_blank',
                    ])->setHtml($icon)
                        ->addText($website);

                    $link = Html::el('div')
                        ->setHtml(sprintf('%s: %s', $type, $href));

                    $websites->addHtml($link);
                }

                return $websites;
            });
        $grid->addColumnText('isActive', 'grid.advertisement-background.overview.is-active')
            ->setAlign('center')
            ->setReplacement($dialYesNo);

        // FILTER
        $grid->addFilterText('name', 'grid.advertisement-background.overview.name');
        $grid->addFilterText('website', 'grid.advertisement-background.overview.website', ['website', 'websiteLeft', 'websiteRight']);
        $grid->addFilterSelect('isActive', 'grid.advertisement-background.overview.is-active', Constant::PROMTP_ARR + Constant::DIAL_YES_NO)
            ->setTranslateOptions();

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.advertisement-background.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.advertisement-background.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['name' => 'ASC']);

        return $grid;
    }
}
