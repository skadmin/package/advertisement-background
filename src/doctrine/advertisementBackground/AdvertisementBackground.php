<?php

declare(strict_types=1);

namespace Skadmin\AdvertisementBackground\Doctrine\Advertisement;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class AdvertisementBackground
{
    use Entity\Id;
    use Entity\Name;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Website;

    #[ORM\Column]
    private string $websiteLeft = '';

    #[ORM\Column]
    private string $websiteRight = '';

    public function update(string $name, bool $isActive, string $website, string $websiteLeft, string $websiteRight, ?string $imagePreview): void
    {
        $this->name         = $name;
        $this->isActive     = $isActive;
        $this->website      = $website;
        $this->websiteLeft  = $websiteLeft;
        $this->websiteRight = $websiteRight;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getWebsiteLeft(bool $force = false): string
    {
        if ($force || $this->websiteLeft !== '') {
            return $this->websiteLeft;
        }

        return $this->getWebsite();
    }

    public function getWebsiteRight(bool $force = false): string
    {
        if ($force || $this->websiteRight !== '') {
            return $this->websiteRight;
        }

        return $this->getWebsite();
    }
}
