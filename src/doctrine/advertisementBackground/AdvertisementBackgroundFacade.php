<?php

declare(strict_types=1);

namespace Skadmin\AdvertisementBackground\Doctrine\Advertisement;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function count;
use function shuffle;

final class AdvertisementBackgroundFacade extends Facade
{
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = AdvertisementBackground::class;
    }

    public function create(string $name, bool $isActive, string $website, string $websiteLeft, string $websiteRight, ?string $imagePreview): AdvertisementBackground
    {
        return $this->update(null, $name, $isActive, $website, $websiteLeft, $websiteRight, $imagePreview);
    }

    public function update(?int $id, string $name, bool $isActive, string $website, string $websiteLeft, string $websiteRight, ?string $imagePreview): AdvertisementBackground
    {
        $advertisementBackground = $this->get($id);
        $advertisementBackground->update($name, $isActive, $website, $websiteLeft, $websiteRight, $imagePreview);

        $this->em->persist($advertisementBackground);
        $this->em->flush();

        return $advertisementBackground;
    }

    public function get(?int $id = null): AdvertisementBackground
    {
        if ($id === null) {
            return new AdvertisementBackground();
        }

        $advertisementBackground = parent::get($id);

        if ($advertisementBackground === null) {
            return new AdvertisementBackground();
        }

        return $advertisementBackground;
    }

    public function getRandom(bool $onlyActive = false): ?AdvertisementBackground
    {
        $advertisementBackgrounds = $this->getAll($onlyActive);

        if (count($advertisementBackgrounds) === 0) {
            return null;
        }

        shuffle($advertisementBackgrounds);

        return $advertisementBackgrounds[0];
    }

    /**
     * @return AdvertisementBackground[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }
}
